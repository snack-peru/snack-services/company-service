package com.snack.controllers;

import java.util.List;

import com.snack.entities.SellerXStore;
import com.snack.services.SellerXStoreService;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.MediaType;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class SellerXStoreController {

    @Autowired
    private SellerXStoreService sellerXStoreService;

    @GetMapping(value = "/api/sellerXStore")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Obtener todos los vendedores registrado por tienda",
            notes = "No recibe ningun parámetro de entrada",
            responseContainer = "Object")
    public List<SellerXStore> getAllSellerXStore() {
        return sellerXStoreService.getAllSellerXStore();
    }

    @PostMapping(value = "/api/sellerXCompany/{idStore}/{idSellerRole}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Registrar vendedor en una tienda en especifico y con un rol asignado",
            notes = "Se debe enviar como parametros el idStore, idSellerRole, y la información restante de sellerXCompany",
            responseContainer = "Object",
            response = SellerXStore.class)
    public SellerXStore saveSellerXCompany(@RequestBody SellerXStore sellerXStore, @PathVariable("idCompany") Integer idStore, @PathVariable("idSellerRole") Integer idSellerRole) {
        return sellerXStoreService.saveSellerXStore(sellerXStore, idStore, idSellerRole);
    }
}