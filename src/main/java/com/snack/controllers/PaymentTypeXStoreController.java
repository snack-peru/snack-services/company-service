package com.snack.controllers;

import com.snack.dto.PaymentTypeDto;
import com.snack.entities.PaymentTypeXStore;
import com.snack.services.PaymentTypeXStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;

@RestController
public class PaymentTypeXStoreController {
    @Autowired
    private PaymentTypeXStoreService paymentTypeXStoreService;

    // Author: Martin
    @GetMapping(value="/api/paymentTypeXStore/{idStore}")
    public List<PaymentTypeDto> getPaymentTypesPerStore(@PathVariable(name="idStore") Long idStore) {
        return paymentTypeXStoreService.getPaymentsTypeForStore(idStore);
    }
}
