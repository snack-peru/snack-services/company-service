package com.snack.controllers;

import java.util.List;

import com.snack.entities.SellerRole;
import com.snack.services.SellerRoleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class SellerRoleController {
    @Autowired
    private SellerRoleService sellerRoleService;

    // Author: Ericsson
    @GetMapping(value = "/api/sellerRole")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Obtener todos los roles de vendedor",
            notes = "No tiene parámetros de entrada",
            responseContainer = "Object")
    public List<SellerRole> getAllSellerRole() {
        return sellerRoleService.getAllSellerRole();
    }

    // Author: Ericsson
    @GetMapping(value = "/api/sellerRole/{idSellerRole}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Obtener un rol de vendedor en especifico",
            notes = "Se debe enviar como parametro el idSellerRole",
            responseContainer = "Object",
            response = SellerRole.class)
    public SellerRole getSellerRoleById(@PathVariable("idSellerRole") Integer idSellerRole) {
        return sellerRoleService.getSellerRoleById(idSellerRole);
    }

    // Author: Ericsson
    @PostMapping(value = "/api/sellerRole")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Registrar rol de vendedor",
            notes = "Se debe enviar como parametro la información del vendedor por medio de un RequestBody",
            responseContainer = "Object",
            response = SellerRole.class)
    public SellerRole saveSellerRole(@RequestBody SellerRole sellerRole) {
        return sellerRoleService.saveSellerRole(sellerRole);
    }

    // Author: Ericsson
    @PatchMapping(value = "/api/sellerRole/{idSellerRole}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Actualizar el estado de un rol de vendedor",
            notes = "Se debe enviar como parametro el nuevo estado del rol indicando el idSellerRole",
            responseContainer = "Object",
            response = Void.class)
    public void updateSellerRoleStatus(@RequestParam(value = "status") Integer status,
            @PathVariable("idSellerRole") Integer idSellerRole) {
        sellerRoleService.updateSellerRoleStatus(status, idSellerRole);
    }
}