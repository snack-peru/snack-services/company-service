package com.snack.controllers;

import java.util.List;

import com.snack.dto.request.PostStoreRequest;
import com.snack.dto.request.PutStoreRequest;
import com.snack.entities.Store;
import com.snack.services.StoreService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/api/store")
public class StoreController {
    @Autowired
    private StoreService storeService;

    // Author: Sergio
    @GetMapping(value = "/{idStore}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Obtener una tienda en especifico",
            notes = "Se debe enviar como parametro idStore",
            responseContainer = "Object",
            response = Store.class)
    public Store getStoreById(@PathVariable("idStore") Integer idStore) {
        return storeService.getStoreById(idStore);
    }


    // Author: Sergio
    @PostMapping
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Registrar una tienda",
            notes = "Se debe enviar como parametro un RequestBody de la informacion de tienda. La fecha de creación" +
                    "se no es necesario pasar, ya que se crea en el mismo backend y la fecha de modificación no pasar",
            responseContainer = "Object",
            response = Store.class)
    public Store saveStore(@RequestBody PostStoreRequest postStoreRequest) {
        return storeService.saveStore(new Store(postStoreRequest));
    }

    @PutMapping
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Actualizar datos de una tienda",
            notes = "Se debe enviar como parametro un RequestBody de la informacion de tienda. La fecha de creación" +
                    "se no es necesario pasar, la fecha de modificación se actualiza dentro del backend",
            responseContainer = "Object",
            response = Store.class)
    public ResponseEntity<?> updateStore(@RequestBody PutStoreRequest putStoreRequest) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(storeService.updateStore(new Store(putStoreRequest)));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @PutMapping(value = "/{address}/{latitude}/{longitude}/{idStore}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Actualizar la direccion de una tienda",
            notes = "Se debe enviar como parametros el address, latitud, longitud y el idStore del la tienda que se va a actualizar",
            responseContainer = "Object",
            response = Void.class)
    public void updateStoreAddress(@PathVariable("address") Integer address,
                                   @PathVariable("latitude") Integer latitude,
                                   @PathVariable("longitude") Integer longitude,
                                   @PathVariable("idStore") Integer idStore) {

        storeService.updateStoreAddress(address, latitude, longitude, idStore);
    }

    @PutMapping(value = "/{idStore}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Actualizar el score de una tienda",
            notes = "Se debe enviar como parametros el score a cambiar y el idStore a actualizar",
            responseContainer = "Object",
            response = Void.class)
    public void updateStoreScore(@RequestParam(value = "score") Integer score,
            @PathVariable("idStore") Integer idStore) {
        storeService.updateStoreScore(score, idStore);
    }

    @PutMapping(value = "/{status}/{idStore}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Actualizar el status de una tienda",
            notes = "Se debe enviar como parametros el status a cambiar y el idStore",
            responseContainer = "Object",
            response = Void.class)
    public void updateStoreStatus(@PathVariable("status") Integer status, @PathVariable("idStore") Integer idStore) {
        storeService.updateStoreStatus(status, idStore);
    }
    
    @GetMapping(value = "/search/{searchkey}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Busca una tienda",
            notes = "Se debe enviar como parametro una variable String ",
            responseContainer = "Object",
            response = Void.class)
    public List<Store> findStores(@PathVariable("searchkey") String searchkey){
        return storeService.findStoreByName(searchkey);
    }



    @GetMapping(value = "/{latitude}/{longitude}/{radiusDistance}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Obtiene todas las bodegas cercanas a la ubicación enviada y en base a la distancia ingresada",
            notes = "Se debe enviar como parametros la latitud, longitud y distancia radial en kilómetros",
            responseContainer = "Object",
            response = Void.class)
    public List<Store> getStoresByCoordinates(@PathVariable("latitude") Double latitude, @PathVariable("longitude") Double longitude, @PathVariable("radiusDistance") Double radiusDistance) {
        return storeService.getStoresByCoordinates(latitude, longitude, radiusDistance);
    }
}