package com.snack.providers;

import com.snack.dto.ListOfIds;
import com.snack.dto.PaymentTypeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class PaymentTypeProvider {
    @Autowired
    private RestTemplate restTemplate;

    private final String ordersUri = "http://orders-service";

    public List<PaymentTypeDto> getPaymentTypesDetail(ListOfIds idList) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<ListOfIds> body = new HttpEntity<ListOfIds>(idList, headers);

        ParameterizedTypeReference<List<PaymentTypeDto>> responseType = new ParameterizedTypeReference <List<PaymentTypeDto>>() {};
        ResponseEntity<List<PaymentTypeDto>> resp = restTemplate.exchange(ordersUri + "/api/paymentType/get-by-ids/", HttpMethod.POST, body, responseType);
        List<PaymentTypeDto> aproxPriceDTOlist = resp.getBody();

        return aproxPriceDTOlist;
    }
}
