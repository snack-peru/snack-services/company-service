package com.snack.repositories;

import com.snack.entities.PaymentTypeXStore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaymentTypeXStoreRepository extends JpaRepository<PaymentTypeXStore, Integer> {
    @Query(value = "select id_payment_type from payment_type_store where id_store = ?1 and status = true", nativeQuery = true)
    public List<Long> findByStore(Long idStore);
}
