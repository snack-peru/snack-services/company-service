package com.snack.repositories;

import java.util.List;

import com.snack.entities.Store;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface StoreRepository extends JpaRepository<Store, Integer> {

    @Transactional
    @Modifying
    @Query(value = "UPDATE store SET address = ?1, latitude = ?2, longitude = ?3 WHERE id_store = ?4", nativeQuery = true)
    public void updateStoreAddress(Integer address, Integer latitude, Integer longitude, Integer idStore);

    @Transactional
    @Modifying
    @Query(value = "UPDATE store SET score = ?1 WHERE id_store = ?2", nativeQuery = true)
    public void updateStoreScore(Integer score, Integer idStore);

    @Transactional
    @Modifying
    @Query(value = "UPDATE store SET status =?1 WHERE id_store = ?2", nativeQuery = true)
    public void updateStoreStatus(Integer status, Integer idStore);

    @Query(value = "SELECT * FROM store WHERE id_status_store <> 3", nativeQuery=true)
    public List<Store> findAllStoreActive();
}