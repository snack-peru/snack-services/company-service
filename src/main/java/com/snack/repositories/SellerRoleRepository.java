package com.snack.repositories;

import com.snack.entities.SellerRole;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface SellerRoleRepository extends JpaRepository<SellerRole, Integer> {
    @Transactional
    @Modifying
    @Query(value = "UPDATE seller_role SET status = ?1 WHERE id_seller_role =?2", nativeQuery = true)
    public void updateSellerRoleStatus(Integer status, Integer idSellerRole);
}