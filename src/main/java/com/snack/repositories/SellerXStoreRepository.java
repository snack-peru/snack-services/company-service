package com.snack.repositories;

import com.snack.entities.SellerXStore;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SellerXStoreRepository extends JpaRepository<SellerXStore, Integer> {

}