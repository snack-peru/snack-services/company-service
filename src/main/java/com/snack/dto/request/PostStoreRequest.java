package com.snack.dto.request;

import com.snack.entities.DeliveryType;
import com.snack.entities.StatusStore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class PostStoreRequest {
    private String name;
    private String ruc;
    private String address;
    private Double latitude;
    private Double longitude;
    private float score;
    private String image;
    private String openTime;
    private String closedTime;
    private Integer status;
    private DeliveryType deliveryType;
    private StatusStore statusStore;
}
