package com.snack.services;

import java.util.List;
import java.util.Optional;

import com.snack.entities.SellerRole;
import com.snack.repositories.SellerRoleRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SellerRoleService {
    @Autowired
    private SellerRoleRepository sellerRoleRepository;

    public List<SellerRole> getAllSellerRole() {
        return sellerRoleRepository.findAll();
    }

    public SellerRole getSellerRoleById(Integer idSellerRole) {
        Optional<SellerRole> optional = sellerRoleRepository.findById(idSellerRole);
        
        if (optional.isPresent()) {
            SellerRole sellerRole = optional.get();
            return sellerRole;
        }

        return null;
    }

    public void updateSellerRoleStatus(Integer status, Integer idSellerRole) {
        sellerRoleRepository.updateSellerRoleStatus(status, idSellerRole);
    }

    public SellerRole saveSellerRole(SellerRole sellerRole) {
        return sellerRoleRepository.save(sellerRole);
    }
}