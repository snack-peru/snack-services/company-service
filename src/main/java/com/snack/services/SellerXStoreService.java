package com.snack.services;

import java.util.List;

import com.snack.entities.SellerRole;
import com.snack.entities.SellerXStore;
import com.snack.entities.Store;
import com.snack.repositories.SellerXStoreRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SellerXStoreService {
    @Autowired
    private SellerXStoreRepository sellerXStoreRepository;

    public List<SellerXStore> getAllSellerXStore() {
        return sellerXStoreRepository.findAll();
    }

    public SellerXStore saveSellerXStore(SellerXStore sellerXStore, Integer idStore, Integer idSellerRole) {
        Store store = new Store();
        store.setIdStore(idStore);

        SellerRole sellerRole = new SellerRole();
        sellerRole.setIdSellerRole(idSellerRole);

        sellerXStore.setStore(store);
        sellerXStore.setSellerRole(sellerRole);
        return sellerXStoreRepository.save(sellerXStore);
    }
}