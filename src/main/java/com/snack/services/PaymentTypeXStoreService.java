package com.snack.services;

import com.snack.dto.ListOfIds;
import com.snack.dto.PaymentTypeDto;
import com.snack.entities.PaymentTypeXStore;
import com.snack.providers.PaymentTypeProvider;
import com.snack.repositories.PaymentTypeXStoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentTypeXStoreService {
    @Autowired
    private PaymentTypeXStoreRepository paymentTypeXStoreRepository;

    @Autowired
    private PaymentTypeProvider paymentTypeProvider;

    public List<PaymentTypeDto> getPaymentsTypeForStore (Long idStore) {
        List<Long> idList =  paymentTypeXStoreRepository.findByStore(idStore);
        ListOfIds listParam = new ListOfIds(idList);
        return paymentTypeProvider.getPaymentTypesDetail(listParam);
    }
}
