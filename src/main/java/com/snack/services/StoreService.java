package com.snack.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Calendar;
import java.util.Optional;

import com.snack.dto.request.PutStoreRequest;
import com.snack.entities.Store;
import java.util.ArrayList;
import java.util.Arrays;
import com.snack.repositories.StoreRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StoreService {
    @Autowired
    private StoreRepository storeRepository;

    public Store getStoreById(Integer idStore) {
        Optional<Store> optional = storeRepository.findById(idStore);

        if (optional.isPresent()) {
            Store store = optional.get();
            return store;
        }
        return null;
    }

    public Store saveStore(Store store) {
        store.setCreationDate(Calendar.getInstance().getTime());
        return storeRepository.save(store);
    }

    public Store updateStore(Store store) throws Exception {
        Optional<Store> optional = storeRepository.findById(store.getIdStore());
        if (optional.isPresent()) {
            return storeRepository.save(optional.get());
        }
        throw new Exception("No existe la tienda/bodega");
    }

    public void updateStoreAddress(Integer address, Integer latitude, Integer longitude, Integer idStore) {
        storeRepository.updateStoreAddress(address, latitude, longitude, idStore);
    }

    public void updateStoreScore(Integer score, Integer idStore) {
        storeRepository.updateStoreScore(score, idStore);
    }

    public void updateStoreStatus(Integer status, Integer idStore) {
        storeRepository.updateStoreStatus(status, idStore);
    }

    public List<Store> findStoreByName(String chain) {
        List<Store> Stores = storeRepository.findAll();
         List<String> SearchKeys = new ArrayList<String>();
         if (!chain.isEmpty()){
             SearchKeys = Arrays.asList((chain.toLowerCase()).split(" "));
         }
         if (SearchKeys.size() > 0){
             List<Store> result = new ArrayList<Store>();
 
             for(Store a : Stores){
                 List<String> storename = new ArrayList<String>();
                 storename.addAll(Arrays.asList((a.getName().toLowerCase()).split(" ")));
                 if(evaluteSearch(storename, SearchKeys)){
                     result.add(a);
                 }
             }
             return result;
         }
         else {
             return Stores;
         }
 
     }

     private Boolean evaluteSearch(List<String> values, List<String> searchKeys){
        List<Integer> booleans = new ArrayList<Integer>();
        Integer temp;

        for(String s: searchKeys){
            temp = 0;
            for(String v: values){
                if(v.contains(s)) 
                temp += 1;

            }
            if(temp>0)
                booleans.add(1);
            else
                booleans.add(0);
        }

            Integer result = 1;
            for(Integer i: booleans)
                result *= i;
        
        return result>0? true : false;
    }

    public List<Store> getStoresByCoordinates(Double latitude, Double longitude, Double radiusDistance) {
        List<Store> stores = storeRepository.findAllStoreActive();
        List<Store> ls = new ArrayList<Store>();

        for(Store s : stores){
            if( (s.getLatitude() >= (latitude - convertKmToLatitude(radiusDistance))) &&
                (s.getLatitude() <= (latitude + convertKmToLatitude(radiusDistance))) &&
                (s.getLongitude() >= (longitude - convertKmToLongitude(radiusDistance, latitude))) &&
                (s.getLongitude() <= (longitude + convertKmToLongitude(radiusDistance, latitude)))
            )
                ls.add(s);
        }
        return ls;
    }

    private Double convertKmToLatitude(Double radiusDistance){
        Double onelatitudeToKM = 110.574;
        return (Double) radiusDistance / onelatitudeToKM;
    }

    private Double convertKmToLongitude(Double radiusDistance, Double latitude){
        Double variableToLongitude = 111.320;
        Double radius = Math.toRadians(latitude);
        return (Double) radiusDistance / (variableToLongitude * Math.cos(radius));
    }

}