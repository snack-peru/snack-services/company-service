package com.snack.entities;


import javax.persistence.*;

@Entity
@Table(name = "payment_type_store")
public class PaymentTypeXStore {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idPaymentTypeXStore")
    private Integer idPaymentTypeXStore;

    @Column(name = "idPaymentType")
    private Integer idPaymentType; //value come from PaymentType table from ordersDb

    @ManyToOne
    @JoinColumn(name = "idStore")
    private Store store;

    @Column(name = "status")
    private Boolean status;

    public PaymentTypeXStore(Integer idPaymentTypeXStore, Integer idPaymentType, Store store, Boolean status) {
        this.idPaymentTypeXStore = idPaymentTypeXStore;
        this.idPaymentType = idPaymentType;
        this.store = store;
        this.status = status;
    }

    public PaymentTypeXStore() {
    }

    @Override
    public String toString() {
        return "PaymentTypeXStore{" +
                "idPaymentTypeXStore=" + idPaymentTypeXStore +
                ", idPaymentType=" + idPaymentType +
                ", store=" + store +
                ", status=" + status +
                '}';
    }

    public Integer getIdPaymentTypeXStore() {
        return idPaymentTypeXStore;
    }

    public void setIdPaymentTypeXStore(Integer idPaymentTypeXStore) {
        this.idPaymentTypeXStore = idPaymentTypeXStore;
    }

    public Integer getIdPaymentType() {
        return idPaymentType;
    }

    public void setIdPaymentType(Integer idPaymentType) {
        this.idPaymentType = idPaymentType;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
