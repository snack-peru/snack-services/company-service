package com.snack.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import com.snack.dto.request.PostStoreRequest;
import com.snack.dto.request.PutStoreRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "store")
public class Store {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idStore")
    private Integer idStore;

    @Column(name = "name")
    private String name;

    @Column(name = "ruc")
    private String ruc;

    @Column(name = "address")
    private String address;

    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "longitude")
    private Double longitude;

    @Column(name = "score")
    private float score;

    @Column(name = "reviews")
    private Integer reviews;

    @Column(name = "image")
    private String image;

    @Column(name = "openTime")
    private String openTime;

    @Column(name = "closedTime")
    private String closedTime;

    @ManyToOne
    @JoinColumn(name = "idDeliveryType")
    private DeliveryType deliveryType;

    @ManyToOne
    @JoinColumn(name = "idStatusStore")
    private StatusStore statusStore;
    
    //campos de auditoria
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd/MM/yyyy")
    @Column(name = "creationDate")
    private Date creationDate;

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd/MM/yyyy")
    @Column(name = "modificationDate")
    private Date modificationDate;

    public Store(PutStoreRequest putStoreRequest) {
        this.idStore = putStoreRequest.getIdStore();
        this.name = putStoreRequest.getName();
        this.ruc = putStoreRequest.getRuc();
        this.address = putStoreRequest.getAddress();
        this.latitude = putStoreRequest.getLatitude();
        this.longitude = putStoreRequest.getLongitude();
        this.score = putStoreRequest.getScore();
        this.image = putStoreRequest.getImage();
        this.openTime = putStoreRequest.getOpenTime();
        this.closedTime = putStoreRequest.getClosedTime();
        this.deliveryType = putStoreRequest.getDeliveryType();
        this.statusStore = putStoreRequest.getStatusStore();
    }

    public Store(PostStoreRequest postStoreRequest) {
        this.name = postStoreRequest.getName();
        this.ruc = postStoreRequest.getRuc();
        this.address = postStoreRequest.getAddress();
        this.latitude = postStoreRequest.getLatitude();
        this.longitude = postStoreRequest.getLongitude();
        this.score = postStoreRequest.getScore();
        this.image = postStoreRequest.getImage();
        this.openTime = postStoreRequest.getOpenTime();
        this.closedTime = postStoreRequest.getClosedTime();
        this.deliveryType = postStoreRequest.getDeliveryType();
        this.statusStore = postStoreRequest.getStatusStore();
    }
}